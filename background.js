var blockeds = [
    'http://*.adtop.vn/*',
    'http://*.adtop.com/*',
    'http://*.scorecardresearch/*',
    'http://*.blueserving.com/*',
    'http://*.eclick.vn/*',
    'http://*.adnetwork.vn/*',
    'http://*.eclick.vn/*',
    'http://*.revsci.net/*',
    'http://*.doubleclick.net/*',
    'http://*.skylink.vn/*',
];

var content = "";
var current_url = "";

//denied load advertisements xhr
chrome.webRequest.onBeforeRequest.addListener(
    function(details) {
        return { cancel: details.url.indexOf("crossdomain.xml") == -1 };
    },
    {
        urls: blockeds
    },
    ["blocking"]
);