var pattern = /hdonline\.vn\/phim(.+?)[0-9]+.html/g;
if(document.URL.match(pattern)) {
	injectScript(chrome.extension.getURL('/gettoken.js'), 'body');
}

function injectScript(file, node) {
    var th = document.getElementsByTagName(node)[0];
    var s = document.createElement('script');
    s.setAttribute('type', 'text/javascript');
    s.setAttribute('src', file);
    th.appendChild(s);
}